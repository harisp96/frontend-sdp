const express = require('express');
const http = require('http')
const path = require('path');

const app = express();

console.log(path.join(__dirname, '/dist/frontend-sdp'));
app.use(express.static(path.join(__dirname, '/dist/frontend-sdp')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/dist/frontend-sdp/index.html'));
});

const port = process.env.PORT || 3000;
app.set('port', port);

const server = http.createServer(app);
server.listen(port, () => console.log('running'));
