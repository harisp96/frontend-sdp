import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ApiService } from '@services/api/api.service';
import { Event } from '@models/event/event.model';
import { Venue } from '@models/venue/venue.model';
import { ToastrService } from 'ngx-toastr';
import { Token } from "@models/token/token.model";

import { trigger, style, animate, transition } from '@angular/animations';


import { ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { } from 'googlemaps';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ opacity: 0}),
          animate('500ms', style({opacity: 1}))
        ]),
        transition(':leave', [
          style({opacity: 1}),
          animate('500ms', style({opacity: 0}))
        ])
      ]
    )
  ],
  styleUrls: ['./event-detail.component.css']
})
export class EventDetailComponent implements OnInit {

  event: Event;
  venue: Venue;
  tab = 1;
  number_of_tickets: number = 1;
  reservation_message = '';
  token;

  constructor(

    private route: ActivatedRoute,
     private apiService: ApiService,
     private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.apiService.authorize();
    var id = this.route.snapshot.paramMap.get('id');
    this.getEvent(id);
    this.getVenueForEvent(id);
    this.getToken();
  }

  getEvent(id): void {
    this.apiService.getEvent(id).subscribe(
        res => {
            console.log('---');
            console.log(res.event);
            console.log('---');
            this.event = new Event().deserialize(res);
            console.log(this.event);

        },
        err => {
            console.log(err.status);
        });
  }

  request_reservation(): void {
    var body = { "ticket": { "event_id": this.route.snapshot.paramMap.get('id'), "description": this.reservation_message, "amount": this.number_of_tickets}};
    this.apiService.request_reservation(body).subscribe(
        res => {
            this.toastr.success("You have sucessfully requested tickets for this event. Check your profile for their status");
            console.log(res);

        },
        err => {
          this.toastr.error(err.error.amount);
        });
  }

    getVenueForEvent(id): void {
      this.apiService.getVenueForEvent(id).subscribe(
          res => {
              console.log('---');
              console.log(res);
              this.venue = new Venue().deserialize(res[0]);
              console.log(this.venue);

          },
          err => {
              console.log(err.status);
          });
    }

    tabSwitch(i){
      this.tab = i;
    }

    getToken(){
      this.token = new Token().getFromLocalStorage();
    }
}
