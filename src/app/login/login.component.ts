import { Component, OnInit } from '@angular/core';
import { AuthService } from '@services/auth/auth.service';
import { Token } from "@models/token/token.model";
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { LoginForm } from '@models/forms/login';
import { SignupForm } from '@models/forms/signup';
import { Router } from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from "@models/user/user.model";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService, private authService: AuthService, private toastr: ToastrService, private router: Router) { }

  login_active: boolean = true;
  loginForm: any;
  login_validation_messages: any;
  signupForm: any;
  signup_validation_messages: any;

  ngOnInit() {
    var token = new Token().getFromLocalStorage();

    if (token != null) {
      this.router.navigate(['/home']);
    }

    this.loginForm = new LoginForm().getForm();
    this.login_validation_messages = new LoginForm().getValidationMessages();
    this.signupForm = new SignupForm().getForm();
    this.signup_validation_messages = new SignupForm().getValidationMessages();
  }

  login(): void {
    this.authService.login(this.loginForm.value.email, this.loginForm.value.password);
  }

  signup(): void {
    //this.signupForm.value.first_name, this.signupForm.value.last_name, this.signupForm.value.username, this.signupForm.value.email, this.signupForm.value.password
    var user = new User().deserialize(this.signupForm.value);

    this.authService.signup(user).subscribe(
        suc => {
          this.toastr.success('Signed up successfully. A confirmation email was sent to you.');
          this.signupForm.reset();
          this.login_active = true;
        },
        err => {
          console.log(err.error);
          for (var key in err.error) {
            for (var value of err.error[key]) {
              this.toastr.error(value);
            }
          }
        }
      );
  }

  changeView(): void {
    this.loginForm.reset();
    this.signupForm.reset();
    this.login_active = !this.login_active;
  }
}
