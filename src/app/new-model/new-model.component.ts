import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '@services/api/api.service';
import { PerformerForm } from '@models/forms/performer';
import { GroupForm } from '@models/forms/group';
import { VenueForm } from '@models/forms/venue'
import { EventForm } from '@models/forms/event'
import { Event } from '@models/event/event.model';
import { Participant } from '@models/participant/participant.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { Venue } from '@models/venue/venue.model';
import { Group } from '@models/group/group.model';
import { Performer } from '@models/performer/performer.model';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from "@angular/router";
import {IMyDpOptions} from 'mydatepicker';
import * as moment from 'moment';



@Component({
  selector: 'app-new-model',
  templateUrl: './new-model.component.html',
  styleUrls: ['./new-model.component.css']
})
export class NewModelComponent implements OnInit {

  public myDatePickerOptions: IMyDpOptions = {
      // other options...
      disableUntil:  {year: parseInt(moment().format('YYYY')), month: parseInt(moment().format('MM')), day: parseInt(moment().format('DD'))},
      dateFormat: 'dd.mm.yyyy',
  };

  location_obj: any;

  loading: boolean = false;
  capacity: number = 10;
  id_of_venue: number = 0;

  selectedItems: any[] = [];
  selectedVenue: Venue;

  createEventStep: number = 1;

  venues: Venue[];
  performers: Performer[];
  groups: Group[];


  lat: any = 51.678418;
  lng: any = 7.809007;

  view: any;
  performerForm: any;
  performer_validation_messages: any;

  groupForm: any;
  group_validation_messages: any;

  venueForm: any;
  venue_validation_messages: any;

  eventForm: any;
  event_validation_messages: any;

  file: any;
  url: any;

  workingEvent: Event;

  constructor(private active_route: ActivatedRoute,private router: Router, private apiService: ApiService, private modalService: NgbModal, private toastr: ToastrService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.resolveView(this.active_route.snapshot.paramMap.get('model'));
    this.performerForm = new PerformerForm().getForm();
    this.performer_validation_messages = new PerformerForm().getValidationMessages();

    this.groupForm = new GroupForm().getForm();
    this.group_validation_messages = new GroupForm().getValidationMessages();

    this.venueForm = new VenueForm().getForm();
    this.venue_validation_messages = new VenueForm().getValidationMessages();

    this.eventForm = new EventForm().getForm();
    this.event_validation_messages = new EventForm().getValidationMessages();

    this.getVenues();
    this.getPerformers();
    this.getGroups();

    console.log(this.selectedItems);
    console.log(this.view);

  }



  getPerformers(){
    this.apiService.getPerformers().subscribe(
      res => {
        console.log(res);
        this.performers = res.map((performer: Performer) => new Performer().deserialize(performer));
      },
      err => {
        console.log(err);
      }
    );
  }

  getGroups(){
    this.apiService.getGroups().subscribe(
      res => {
        this.groups = res.map((group: Group) => new Group().deserialize(group));
      },
      err => {
        console.log(err);
      }
    );
  }

  addGroup(group){
    var participant = new Participant(group.id, 'Group');
    this.apiService.addPerformerToEvent(this.workingEvent, participant).subscribe(suc => {
      console.log('added');
      console.log(suc);
      this.getGroupsExcludingEvent();
      this.getPerformersExcludingEvent();

    }, err => {
      console.log(err);
    });
  }

  addPerformer(performer){
    var participant = new Participant(performer.id, 'Performer');
    this.apiService.addPerformerToEvent(this.workingEvent, participant).subscribe(suc => {
      console.log('added');
      console.log(suc);
      this.getGroupsExcludingEvent();
      this.getPerformersExcludingEvent();

    }, err => {
      console.log(err);
    });
  }


  addVenue(){
    console.log('evo me tu');
    console.log(this.workingEvent);

    this.workingEvent.venue_id = this.selectedVenue.id;
    this.workingEvent.number_of_tickets = this.capacity;
    console.log(this.workingEvent);
    this.apiService.patchEvent(this.workingEvent).subscribe(suc => {
      console.log(suc);
      this.toastr.success("Venue Added. Continue to step 3");
      this.createEventStep++;
    },
    err => {
      console.log(err);
    });
  }

  venueClick(i,venue_id){

    this.id_of_venue = venue_id;
    this.selectedVenue = this.venues[i];
    this.capacity = this.selectedVenue.capacity;

    this.lat = this.selectedVenue.latitude;
    this.lng = this.selectedVenue.longitude;
  }

  valuechange(event){
    if (event.length < 3 ) {
      return;
    }
    this.apiService.getAddressToGeoLocation(event).subscribe(
      res => {
        if (res.status == 'OK') {

          this.location_obj = res.results[0];
          console.log(this.location_obj);
          this.lat = res.results[0].geometry.location.lat;
          this.lng = res.results[0].geometry.location.lng;
          console.log('uredu response');
        }
        else {
          console.log('nije uredu nema rez');
        }
      },
      err => {console.log('erorr');
    }
    );

  }

  createPerformer() {
    this.getBase64(this.file).then(
      data => {
        this.performerForm.patchValue({base64_photo: data});
        var performer = new Performer().deserialize(this.performerForm.value);
        this.apiService.createPerformer(performer).subscribe(suc => {
          console.log(suc);
          this.router.navigate(['/performers/' + suc["id"]]);
        },
        err => {
          this.toastr.error(err);
        });
      },
      err => {
        this.toastr.error(err);
      }
    );

  }

  createGroup(){
    this.getBase64(this.file).then(
      data => {
        this.groupForm.patchValue({base64_photo: data});
        var group = new Group().deserialize(this.groupForm.value);
        console.log(group);
        this.apiService.createGroup(group).subscribe(suc => {
          console.log(suc);
          this.router.navigate(['/groups/' + suc.id]);
        },
        err => {
          this.toastr.error(err);
        });
      },
      err => {
        this.toastr.error(err);
      }
    );

  }

  createVenue(){
    this.venueForm.patchValue({latitude: this.lat, longitude: this.lng, address: this.location_obj.formatted_address});
    this.getBase64(this.file).then(
      data => {
        this.venueForm.patchValue({base64_photo: data});
        var venue = new Venue().deserialize(this.venueForm.value);
        console.log(venue);
        this.apiService.createVenue(venue).subscribe(suc => {
          console.log(suc);
          this.router.navigate(['/venues/' + suc.id]);
        },
        err => {
          this.toastr.error(err);
        });
      },
      err => {
        this.toastr.error(err);
      }
    );
  }

  onFileChanged(event) {
    const file = event.target.files[0];
    console.log(event);
    console.log(file);
    this.file = file;
    this.performerForm.patchValue({current_file: file.name});
    this.groupForm.patchValue({current_file: file.name});
    this.venueForm.patchValue({current_file: file.name});
    this.eventForm.patchValue({current_file: file.name});

    var reader = new FileReader();

    reader.readAsDataURL(file);

    reader.onload = (event) => {
      this.url = reader.result;
    }

    reader.onerror = () => {
      this.url = null;
    }
  }

  getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      if (typeof file === 'undefined'){
        reject('File is empty or not an image!');
      }
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result.split(',')[1]);
      reader.onerror = error => reject(error);
    });
  }

  clearFile(){
    this.file = null;
    this.url = null;
    this.performerForm.patchValue({current_file: null});
    this.groupForm.patchValue({current_file: null});
    this.venueForm.patchValue({current_file: null});
    this.eventForm.patchValue({current_file: null});

  }

  open(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  getPerformersExcludingEvent(){
    this.apiService.getPerformersExcludingEvent(this.workingEvent).subscribe(
      res => {
        console.log(res);
        this.performers = res.map((performer: Performer) => new Performer().deserialize(performer));
      },
      err => {
        console.log(err);
      }
    );
  }

  getGroupsExcludingEvent(){
    this.apiService.getGroupsExcludingEvent(this.workingEvent).subscribe(
      res => {
        this.groups = res.map((group: Group) => new Group().deserialize(group));
      },
      err => {
        console.log(err);
      }
    );
  }



  getVenues(): void {
    this.apiService.getVenues().subscribe(
        res => {
            console.log('get venues');
            console.log(res.venues);
            this.venues = res.map((venue: Venue) => new Venue().deserialize(venue));
            console.log(this.venues);

        },
        err => {
            console.log(err.status);
        });
  }

  createEvent(): void {
    this.eventForm.patchValue({date: this.eventForm.value.date.formatted});
    console.log(this.eventForm.value);
    this.getBase64(this.file).then(
      data => {
        this.eventForm.patchValue({base64_photo: data});
        var event = new Event().deserialize(this.eventForm.value);
        console.log(event);
        this.loading = true;
        this.apiService.createEvent(event).subscribe(suc => {
          this.loading = false;
          console.log('response');
          console.log(suc);

          this.workingEvent = new Event().deserialize(suc);
          console.log('workingEvent');
          console.log(this.workingEvent);
          this.toastr.success("Event created. Continue to step 2");
          this.createEventStep++;
        },
        err => {
          this.loading = false;
          this.toastr.error(err);
        });
      },
      err => {
        this.toastr.error(err);
      }
    );

  }

  nextStep(): void {
    if (this.createEventStep == 3) {
      console.log('kreiraj event');
    }
    else {
      this.createEventStep++;
    }
  }


  resolveView(model){
    switch(model) {
      case 'event': {
        this.view = 1;
        return;
      }
      case 'performer': {
        this.view = 2;
        return;

      }
      case 'group': {
        this.view = 3;
        return;

      }
      case 'venue': {
        this.view = 4;
        return;

      }
      default: {
        this.view = 0;
        return;

      }
    }

  }


}
