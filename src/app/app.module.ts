import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';
import { AppComponent } from '@app/app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule } from '@angular/forms';
import { MomentModule } from 'angular2-moment';
import { NgxSpinnerModule } from 'ngx-spinner';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { PerformerDetailComponent } from './performer-detail/performer-detail.component';
import { GroupDetailComponent } from './group-detail/group-detail.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AgmCoreModule } from '@agm/core';
import { NgxLoadingModule } from 'ngx-loading';
import { NewModelComponent } from './new-model/new-model.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EditModelComponent } from './edit-model/edit-model.component';
import { VenueDetailComponent } from './venue-detail/venue-detail.component';
import { ProfileComponent } from './profile/profile.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { ExploreComponent } from './explore/explore.component';
import { LogoutComponent } from './logout/logout.component';
import { MyDatePickerModule } from 'mydatepicker';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    EventDetailComponent,
    PerformerDetailComponent,
    GroupDetailComponent,
    NewModelComponent,
    EditModelComponent,
    VenueDetailComponent,
    ProfileComponent,
    MainNavComponent,
    ExploreComponent,
    LogoutComponent,
  ],
  imports:
  [
    NgbModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      maxOpened: 5,
      autoDismiss: true
    }),
    BrowserModule,
    FormsModule,
    NgxSpinnerModule,
    MyDatePickerModule,
    NgxLoadingModule.forRoot({}),
    MomentModule,
    AngularFontAwesomeModule,
    AppRoutingModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDuvZ9YTExOlxpEYvLnllnLqTwEFlZY_fs',
      libraries: ["places"]
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
