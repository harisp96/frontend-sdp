import { Component, OnInit } from '@angular/core';
import { Venue } from '@models/venue/venue.model';
import { Event } from '@models/event/event.model';

import { ActivatedRoute } from '@angular/router';
import { ApiService } from '@services/api/api.service';

@Component({
  selector: 'app-venue-detail',
  templateUrl: './venue-detail.component.html',
  styleUrls: ['./venue-detail.component.css']
})
export class VenueDetailComponent implements OnInit {

  venue: Venue;
  events: Event[];

  constructor(private active_route: ActivatedRoute, private apiService: ApiService ) { }

  ngOnInit() {
    this.apiService.authorize();
    var id = this.active_route.snapshot.paramMap.get('id');
    this.getVenue(id);
    this.getEventsForVenue(id);
  }

  getVenue(id){
    this.apiService.getVenue(id).subscribe(
      res => {
        console.log(res);
        this.venue = new Venue().deserialize(res);
        console.log(this.venue);

      },
      err => {
        console.log('error');
      }
    );
  }

  getEventsForVenue(id){
    this.apiService.getEventsForVenue(id).subscribe(
      res => {
        this.events = res.map((event: Event) => new Event().deserialize(event));
      },
      err => {
        console.log(err);
      }
    );
  }

}
