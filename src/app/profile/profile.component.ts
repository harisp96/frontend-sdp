import { Component, OnInit } from '@angular/core';
import { User } from '@models/user/user.model';
import { Token } from '@models/token/token.model';
import { ApiService } from '@services/api/api.service';
import { trigger, style, animate, transition } from '@angular/animations';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ opacity: 0}),
          animate('800ms', style({opacity: 1}))
        ]),
        transition(':leave', [
          style({opacity: 1}),
          animate('200ms', style({opacity: 0}))
        ])
      ]),
      trigger('mainAnimation', [
          transition(':enter', [
            style({ opacity: 0}),
            animate('800ms', style({opacity: 1}))
          ])
        ])
  ],
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: User;
  requests_for_me: any[];
  my_requests: any[];
  file: any;
  url: any;

  constructor(private apiService: ApiService, private toastr: ToastrService) { }

  ngOnInit() {
    this.apiService.authorize();
    var id = new Token().getFromLocalStorage().user_id;
    this.getUserProfile(id);
    this.getmyRequests(id);
    this.getRequestsforMe(id);
  }

  onFileChanged(event) {
    const file = event.target.files[0];
    console.log(event);
    console.log(file);
    this.file = file;

    var reader = new FileReader();

    reader.readAsDataURL(file);

    reader.onload = (event) => {
      this.url = reader.result;
      this.patchProfilePhoto();
    }

    reader.onerror = () => {
      this.url = null;
    }
  }

  patchProfilePhoto() {
    this.getBase64(this.file).then(
      data => {
        var image = { base64_photo: data };

        this.apiService.patchUserPhoto(image, this.user.id).subscribe(suc => {
          console.log(suc);
          var id = new Token().getFromLocalStorage().user_id;
          this.getUserProfile(id);
        },
        err => {
          console.log(err);
        });
      },
      err => {
        console.log(err);
      }
    );
  }

  getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      if (typeof file === 'undefined'){
        reject('File is empty or not an image!');
      }
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result.split(',')[1]);
      reader.onerror = error => reject(error);
    });
  }

  getmyRequests(id){
    this.apiService.getmyRequests(id).subscribe(
        res => {
          console.log('my requests');
            console.log(res);
            this.my_requests = res.map((ticket: any) => ticket );
            console.log(this.my_requests);
        },
        err => {
            console.log(err.status);
        });
  }

  getColor(status){
    if (status == 'pending')
    {
      return 'orange';
    }
    else if (status == 'accepted')
    {
      return 'green';
    }
    else return 'red';
  }

  getRequestsforMe(id){
    this.apiService.getRequestsforMe(id).subscribe(
        res => {
          console.log('my requests');
            console.log(res);
            this.requests_for_me = res.map((ticket: any) => ticket );
            console.log(this.requests_for_me);
        },
        err => {
            console.log(err.status);
        });
  }

  getUserProfile(id): void {
    this.apiService.getUserProfile(id).subscribe(
      res => {
          console.log('get user');
          console.log(res);
          this.user = new User().deserialize(res);
      },
      err => {
          console.log(err.status);
      }
    );
  }

  confirmRequest(id): void {
    this.apiService.confirmRequest(id).subscribe(
      suc => {
        var uid = new Token().getFromLocalStorage().user_id;
        this.getRequestsforMe(uid);
      },
      err => {
        this.toastr.error(err.error.message);
      }
    );
  }

  rejectRequest(id): void {
    this.apiService.rejectRequest(id).subscribe(
      suc => {
        var uid = new Token().getFromLocalStorage().user_id;
        this.getRequestsforMe(uid);
      },
      err => {
        console.log('err');
      }
    );
  }

  getDate(date){
     return moment(date).format("MMM Do YY"); ;
   }

}
