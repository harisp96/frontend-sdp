import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';
import { LoginComponent }           from '@app/login/login.component';
import { AppComponent }             from '@app/app.component';
import { DashboardComponent }       from '@app/dashboard/dashboard.component';
import { EventDetailComponent }     from '@app/event-detail/event-detail.component';
import { GroupDetailComponent }     from '@app/group-detail/group-detail.component';
import { PerformerDetailComponent } from '@app/performer-detail/performer-detail.component';
import { NewModelComponent }        from '@app/new-model/new-model.component';
import { EditModelComponent }       from '@app/edit-model/edit-model.component';
import { VenueDetailComponent }     from '@app/venue-detail/venue-detail.component';
import { ProfileComponent }         from '@app/profile/profile.component';
import { MainNavComponent }         from '@app/main-nav/main-nav.component';
import { ExploreComponent }         from '@app/explore/explore.component';
import { LogoutComponent }          from '@app/logout/logout.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent},
  { path: 'events/:id', component: EventDetailComponent },
  { path: 'performers/:id', component: PerformerDetailComponent },
  { path: 'groups/:id', component: GroupDetailComponent },
  { path: 'venues/:id', component: VenueDetailComponent },
  { path: 'new/:model', component: NewModelComponent },
  { path: ':model/:id/edit', component: EditModelComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'home', component: MainNavComponent },
  { path: 'explore', component: ExploreComponent },
  { path: 'logout', component: LogoutComponent },

];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],
})
export class AppRoutingModule {}
