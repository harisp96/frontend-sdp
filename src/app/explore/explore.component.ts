import { Component, OnInit } from '@angular/core';
import { Event } from '@models/event/event.model';
import { Performer } from '@models/performer/performer.model';
import { Group } from '@models/group/group.model';
import { Venue } from '@models/venue/venue.model';
import { ApiService } from '@services/api/api.service';
import { trigger, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-explore',
  templateUrl: './explore.component.html',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ opacity: 0}),
          animate('800ms', style({opacity: 1}))
        ]),
        transition(':leave', [
          style({opacity: 1}),
          animate('200ms', style({opacity: 0}))
        ])
      ]),
      trigger('mainAnimation', [
          transition(':enter', [
            style({ opacity: 0}),
            animate('800ms', style({opacity: 1}))
          ])
        ])
  ],
  styleUrls: ['./explore.component.scss']
})
export class ExploreComponent implements OnInit {

  groups: Group[];
  events: Event[];
  performers: Performer[];
  venues: Venue[];
  tab;


  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.authorize();
    this.tab = 0;
    this.getEvents();
    this.getPerformers();
    this.getGroups();
    this.getVenues();
  }

  getUrl(object): string {
    console.log(object)
    return 'url('+ object.featured_photo + ')';
  }

  changeView(tab): void {
    this.tab = tab;
    console.log('Switching to tab ' + this.tab);
  }

    getVenues(): void {
      this.apiService.getVenues().subscribe(
          res => {
              console.log('got venues');
              console.log(res.venues);
              this.venues = res.map((venue: Venue) => new Venue().deserialize(venue));


          },
          err => {
              console.log(err.status);
          });

    }

    getEvents(): void {
      this.apiService.getEvents().subscribe(
          res => {
              this.events = res.map((event: Event) => new Event().deserialize(event));

          },
          err => {
              console.log(err.status);
          });

    }

    getPerformers(): void {
      this.apiService.getPerformers().subscribe(
          res => {
              console.log(res.performers);
              this.performers = res.map((performer: Performer) => new Performer().deserialize(performer));
              console.log(this.performers);

          },
          err => {
              console.log(err.status);
          });
    }

    getGroups(): void {
      this.apiService.getGroups().subscribe(
          res => {
              console.log('get groups');
              console.log(res.groups);
              this.groups = res.map((group: Group) => new Group().deserialize(group));
              console.log(this.groups);

          },
          err => {
              console.log(err.status);
          });
    }

}
