import { Component, OnInit } from '@angular/core';
import { Event } from '@models/event/event.model';
import { Performer } from '@models/performer/performer.model';
import { Group } from '@models/group/group.model';
import { User } from '@models/user/user.model';
import { Token } from '@models/token/token.model';

import { Venue } from '@models/venue/venue.model';
import * as moment from 'moment';
import { Activity } from '@models/activity/activity.model';
import { ApiService } from '@services/api/api.service';
import {Router} from "@angular/router"
import { trigger, style, animate, transition } from '@angular/animations';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ opacity: 0}),
          animate('800ms', style({opacity: 1}))
        ]),
        transition(':leave', [
          style({opacity: 1}),
          animate('200ms', style({opacity: 0}))
        ])
      ]),
      trigger('mainAnimation', [
          transition(':enter', [
            style({ opacity: 0}),
            animate('800ms', style({opacity: 1}))
          ])
        ])
  ],
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public loading = true;
  groups: Group[];
  events: Event[];
  performers: Performer[];
  activities: Activity[];
  venues: Venue[];
  user: User;
  tab;
  news: any[];
  my_news: any[];
  mainNav: number = 0;
  closeResult: string;
  requests_for_me: any[];
  my_requests: any[];

  constructor(private router: Router,  private apiService: ApiService, private modalService: NgbModal ) { }

  ngOnInit() {
    this.apiService.authorize();
    var id = new Token().getFromLocalStorage().user_id;
    this.tab = 0;
    this.getNews();
    }

  logout(): void {
    this.apiService.logout();
    }

  getNews(): void {
    this.apiService.getNews().subscribe(
        res => {
          console.log('newsovi');
            console.log(res);
            this.news = res.map((venue: any) => venue );
            console.log(this.news);
        },
        err => {
            console.log(err.status);
        });
  }

  parseDate(date){
    return moment(date).fromNow();
  }

  getUrl(object): string {
    console.log(object)
    return 'url('+ object.featured_photo + ')';
  }

}
