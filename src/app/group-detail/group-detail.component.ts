import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ApiService } from '@services/api/api.service';
import { Group } from '@models/group/group.model';
import { Event } from '@models/event/event.model';
import { Performer } from '@models/performer/performer.model';
import { ToastrService } from 'ngx-toastr';
import { Token } from "@models/token/token.model";


@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.css']
})
export class GroupDetailComponent implements OnInit {

  group: Group;
  events: Event[];
  performers: Performer[];
  performersNotInGroup: Performer[];
  adding: boolean = false;
  id: any;
  text = 'Add performers to group';
  token;




  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private toastr: ToastrService
    ) { }

  ngOnInit() {
    this.apiService.authorize();
    console.log('group-detail');
    this.id = this.route.snapshot.paramMap.get('id');
    this.getGroup(this.id);
    this.getGroupEvents(this.id);
    this.getGroupPerformers(this.id);
    this.getPerformersNotInGroup(this.id);
    this.getToken();

  }

  getToken(){
    this.token = new Token().getFromLocalStorage();
  }

  changeView() {
    this.adding = !this.adding;
    if(this.adding == true) {
      this.text = 'See performers in the group';
    }
    else {
      this.text = 'Add performers to the group';
    }
  }

  getGroup(id): void {
    this.apiService.getGroup(id).subscribe(
        res => {
            console.log('---');
            console.log(res);
            console.log('---');
            this.group = new Group().deserialize(res);
            console.log(this.group);

        },
        err => {
            console.log(err.status);
        });
  }


    getGroupEvents(id): void {
      this.apiService.getGroupEvents(id).subscribe(
          res => {
              console.log(res.events);
              this.events = res.map((event: Event) => new Event().deserialize(event));
              console.log('sto nije array eventa');

              console.log(this.events);

          },
          err => {
              console.log(err.status);
          });

    }
    getGroupPerformers(id): void {
      this.apiService.getGroupPerformers(id).subscribe(
          res => {
              console.log('get perfs');
              console.log(res);
              this.performers = res.map((group: Performer) => new Performer().deserialize(group));
              console.log(this.performers);

          },
          err => {
              console.log(err.status);
          });
    }

    getPerformersNotInGroup(id): void {
      this.apiService.getPerformersNotInGroup(id).subscribe(
          res => {
              console.log('get perfs');
              console.log(res);
              this.performersNotInGroup = res.map((group: Performer) => new Performer().deserialize(group));
              console.log(this.performersNotInGroup);

          },
          err => {
              console.log(err.status);
          });
    }

    addPerformerToGroup(p_id){
      this.apiService.addPerformerToGroup(this.id, p_id).subscribe(
          res => {
            this.toastr.success("Successfully added to group");
            this.getPerformersNotInGroup(this.id);
            this.getGroupPerformers(this.id);

          },
          err => {
            this.toastr.error(err.error.message);
            console.log(err);
          });
    }
}
