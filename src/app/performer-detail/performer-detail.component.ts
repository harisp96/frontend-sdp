import { Component, OnInit } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import { Performer } from '@models/performer/performer.model';
import { Group } from '@models/group/group.model';
import { Event } from '@models/event/event.model';

import { ApiService } from '@services/api/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-performer-detail',
  templateUrl: './performer-detail.component.html',
  styleUrls: ['./performer-detail.component.css']
})
export class PerformerDetailComponent implements OnInit {

  show:boolean = false;
  performer: Performer;
  events: Event[];
  groups: Group[];


  constructor(private apiService: ApiService, private active_route: ActivatedRoute) { }

  ngOnInit() {
    this.apiService.authorize();
    var id = this.active_route.snapshot.paramMap.get('id');
    this.getPerformer(id);
    this.getPerformerEvents(id);
    this.getPerformerGroups(id);
  }

  getPerformer(id){
    this.apiService.getPerformer(id).subscribe(
      res => {
        this.performer = new Performer().deserialize(res);
        console.log(this.performer);
      }
    );
  }

  getPerformerEvents(id): void {
    this.apiService.getPerformerEvents(id).subscribe(
        res => {
            console.log(res.events);
            this.events = res.map((event: Event) => new Event().deserialize(event));

            console.log(this.events);

        },
        err => {
            console.log(err.status);
        });

  }
  getPerformerGroups(id): void {
    this.apiService.getPerformerGroups(id).subscribe(
        res => {
            console.log('get groups');
            console.log(res.groups);
            this.groups = res.map((group: Group) => new Group().deserialize(group));
            console.log(this.groups);

        },
        err => {
            console.log(err.status);
        });
  }

}
