/** A hero's name can't match the hero's alter ego */
import { FormGroup, FormControl } from '@angular/forms';
import { Validators, ValidationErrors, ValidatorFn } from '@angular/forms';

export const PasswordConfirmValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const password = control.get('password').value;
  const password_confirm = control.get('password_confirm').value;

  return password!=password_confirm ? { 'password_confirm': true } : null;
};
