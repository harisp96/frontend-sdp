import { Deserializable } from "@shared/models/deserializable.model"

export class User implements Deserializable {

  id: number;
  first_name: string;
  last_name: string;
  username: string;
  email: string;
  password: string;
  featured_photo: string;
  venues: number;
  performers: number;
  groups: number;
  events: number;

  deserialize(input: any) {
   Object.assign(this, input);
   return this;
 }

 has_image(url) {
   if (url) {
     return false;
   }

   if (this.featured_photo) {
     return true;
   }
   return false;
 }
 // constructor(first_name: string, last_name: string, username: string, email: string, password: string) {
 //   this.first_name = first_name;
 //   this.last_name = last_name;
 //   this.username = username;
 //   this.email = email;
 //   this.password = password;
 // }

}
