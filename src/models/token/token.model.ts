import { Deserializable } from "@shared/models/deserializable.model"

export class Token implements Deserializable {
  access_token: string;
  token_type: string;
  created_at: number;
  expires_in: number;
  refresh_token: string;
  user_id: number;

  deserialize(input: any) {
   Object.assign(this, input);
   return this;
 }

  showToken() {
    console.log('ti si genije');
    console.log(this);
  }

  saveToLocalStorage() {
    localStorage.setItem('token', JSON.stringify(this));
  }

  getFromLocalStorage(){
    var token = JSON.parse(localStorage.getItem("token"));
    console.log('token' + token);
    if (token != null) {
      Object.assign(this, token);
      return this;
    }
    else {
      return null;
    }

  }
}
