import { Deserializable } from "@shared/models/deserializable.model"

export class Participant implements Deserializable {

  resource_id: number;
  resource_type: string;

  deserialize(input: any) {
   Object.assign(this, input);
   return this;
 }

 constructor(resource_id, resource_type) {
   this.resource_id = resource_id;
   this.resource_type = resource_type;
 }



  // constructor(first_name: string, last_name: string, bio: string, base64_photo: string) {
  //   this.first_name = first_name;
  //   this.last_name = last_name;
  //   this.bio = bio;
  //   this.base64_photo = base64_photo;
  // }




}
