import { Deserializable } from "@shared/models/deserializable.model"

export class Group implements Deserializable {

  first_name: string;
  last_name: string;
  bio: string;
  base64_photo: string;
  user_id: number;

  deserialize(input: any) {
   Object.assign(this, input);
   return this;
 }

}
