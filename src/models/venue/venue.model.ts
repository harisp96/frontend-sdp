import { Deserializable } from "@shared/models/deserializable.model"

export class Venue implements Deserializable {

  id: number;
  name: string;
  description: string;
  base64_photo: string;
  latitude: string;
  longitude: string;
  capacity: number;


  deserialize(input: any) {
   Object.assign(this, input);
   return this;
 }



  // constructor(first_name: string, last_name: string, bio: string, base64_photo: string) {
  //   this.first_name = first_name;
  //   this.last_name = last_name;
  //   this.bio = bio;
  //   this.base64_photo = base64_photo;
  // }




}
