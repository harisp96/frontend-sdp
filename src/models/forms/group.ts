import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';

export class GroupForm {

  getForm(){
    var form;
    return form = new FormGroup({
        name: new FormControl('', [
        Validators.required
        ]),
        bio: new FormControl('', [
        Validators.required
      ]),
      base64_photo: new FormControl(''),
      current_file: new FormControl(''),
    });
  }

  getValidationMessages(){
    var validation_messages = {
     'name': [
       { type: 'required', message: 'Name cannot be blank' }
     ],
     'bio': [
       { type: 'required', message: 'Bio cannot be blank' }
     ]
   };
   return validation_messages;
 }


}
