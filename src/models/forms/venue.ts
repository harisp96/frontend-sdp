import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';

export class VenueForm {

  getForm(){
    var form;
    return form = new FormGroup({
        name: new FormControl('', [
        Validators.required
        ]),
        address: new FormControl('', [
        Validators.required
        ]),
        description: new FormControl('', [
        Validators.required
      ]),
      capacity: new FormControl('', [
      Validators.required, Validators.min(1), Validators.pattern('^[0-9]*$')
      ]),
      base64_photo: new FormControl(''),
      latitude: new FormControl(''),
      longitude: new FormControl(''),
      current_file: new FormControl(''),
    });
  }

  getValidationMessages(){
    var validation_messages = {
     'name': [
       { type: 'required', message: 'Name cannot be blank' }
     ],
     'description': [
       { type: 'required', message: 'Description cannot be blank' }
     ],
     'address': [
       { type: 'required', message: 'Address cannot be blank' }
     ],
     'capacity': [
       { type: 'required', message: 'Capacity cannot be blank' },
       { type: 'min', message: 'Too small'},
       { type: 'pattern', message: 'Not a valid number'}
     ]
   };
   return validation_messages;
 }


}
