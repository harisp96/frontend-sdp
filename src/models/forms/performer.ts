import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';

export class PerformerForm {

  getForm(){
    var form;
    return form = new FormGroup({
        first_name: new FormControl('', [
        Validators.required
        ]),
        last_name: new FormControl('', [
        Validators.required
      ]),
        bio: new FormControl('', [
        Validators.required
      ]),
      base64_photo: new FormControl(''),
      current_file: new FormControl(''),
    });
  }

  getValidationMessages(){
    var validation_messages = {
     'first_name': [
       { type: 'required', message: 'First name cannot be blank' }
     ],
     'last_name': [
       { type: 'required', message: 'Last name cannot be blank' }
     ],
     'bio': [
       { type: 'required', message: 'Bio cannot be blank' }
     ]
   };
   return validation_messages;
 }


}
