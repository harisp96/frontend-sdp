import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';

export class LoginForm {

  getForm(){
    var form;
    return form = new FormGroup({
        email: new FormControl('', [
        Validators.required
        ]),
        password: new FormControl('', [
        Validators.required
      ]),
    });
  }

  getValidationMessages(){
    var validation_messages = {
     'email': [
       { type: 'required', message: 'Email cannot be blank' }
     ],
     'password': [
       { type: 'required', message: 'Password cannot be blank' }
     ]
   };
   return validation_messages;
 }


}
