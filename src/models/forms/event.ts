import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';

export class EventForm {

  getForm(){
    var form;
    return form = new FormGroup({
        name: new FormControl('', [
        Validators.required
        ]),
        description: new FormControl('', [
        Validators.required
      ]),
      base64_photo: new FormControl(''),
      current_file: new FormControl(''),
      venue_id: new FormControl(''),
      date: new FormControl('')
    });
  }

  getValidationMessages(){
    var validation_messages = {
     'name': [
       { type: 'required', message: 'Name cannot be blank' }
     ],
     'description': [
       { type: 'required', message: 'Description cannot be blank' }
     ]
   };
   return validation_messages;
 }


}
