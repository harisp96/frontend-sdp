
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { PasswordConfirmValidator } from '@shared/validator/password-confirm.directive';

export class SignupForm {

  getForm(){
    var form;
    return form = new FormGroup({
        first_name: new FormControl('', [
        Validators.required
        ]),
        last_name: new FormControl('', [
        Validators.required
        ]),
        username: new FormControl('', [
        Validators.required,
        Validators.minLength(3)
        ]),
        email: new FormControl('', [
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')
        ]),
        password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
        ]),
        password_confirm: new FormControl('', [
        Validators.required,
      ]),
    }, { validators: PasswordConfirmValidator });
  }

  getValidationMessages(){
    var validation_messages = {
      'first_name': [
        { type: 'required', message: 'First name cannot be blank' }
      ],
      'last_name': [
        { type: 'required', message: 'Last name cannot be blank' },

      ],
      'username': [
        { type: 'required', message: 'Username cannot be blank' },
        { type: 'minlength', message: 'Your username must be longer than 2 characters' }

      ],
     'email': [
       { type: 'required', message: 'Email cannot be blank' },
       { type: 'pattern', message: 'Email you typed in is not valid' }

     ],
     'password': [
       { type: 'required', message: 'Password cannot be blank' },
       { type: 'minlength', message: 'Your password is too short (8+)' },
     ]
   };
   return validation_messages;
 }


}
