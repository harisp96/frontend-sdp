import { Deserializable } from "@shared/models/deserializable.model"
import * as moment from 'moment';

export class Activity implements Deserializable {

  resource_type: string;
  created_at: string;

  deserialize(input: any) {
   Object.assign(this, input);
   return this;
 }

 getText(): string {
   var text = this + 'created' + this.resource_type + ' ' + moment(this.created_at).fromNow();
   console.log(text);
   return text;
 }

 getDate(): string {
   return moment(this.created_at).fromNow();
 }

 getClassForIcon(): string{
   switch(this.resource_type) {
     case 'Event': {
       return 'fa-calendar';
     }
     case 'Performer': {
       return 'fa-user';
     }
     case 'Group': {
       return 'fa-users';
     }
     case 'Comment': {
       return 'fa-comment';
     }
     case 'Like': {
       return 'fa-thumbs-up';
     }
     case 'Venue': {
       return 'fa-map-marker';
     }
     default: {
       return 'fa-notes-medical';
     }
   }
 }

}
