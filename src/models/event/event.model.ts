import { Deserializable } from "@shared/models/deserializable.model"
import * as moment from 'moment';

export class Event implements Deserializable {

  description: string;
  name: string;
  id: number;
  photos: string[];
  number_of_tickets: number;
  tickets_remaining: number;
  date: string;
  venue_id: number;

  deserialize(input: any) {
   Object.assign(this, input);
   return this;
 }

 getDate(){
   return moment(this.date).format("MMM Do YY"); ;
 }
}
