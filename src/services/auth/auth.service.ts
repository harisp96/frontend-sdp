import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'environments/environment';
import { Token } from "@models/token/token.model";
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})


export class AuthService {

  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService) { }

  public token = new Token().getFromLocalStorage();
  public BASE_URL = environment.baseUrl;
  public HEADERS = { headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }}


  checkForLogin(){
    this.token = new Token().getFromLocalStorage();
    if (this.token != null){
      this.checkForExpiry();  // navigate to dashboard if user tries to go to login already logged in
      this.router.navigate(['/dashboard']);
    }
    else {
      this.router.navigate(['/login']);
    }
  }

  authorize() {
    this.token = new Token().getFromLocalStorage();
    if (this.token != null){
      this.checkForExpiry();  // navigate to dashboard if user tries to go to login already logged in
    }
    else {
      this.router.navigate(['/login']);
    }
  }

  send_file(file){


    console.log(file);
    var params = JSON.stringify({photo: {base64: file}});
    this.http.post(this.BASE_URL + '/photos', params, { headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }})
      .subscribe(
        suc => {
            console.log(suc);
        },
        err => {
            console.log(err);
        });
  }

  checkForExpiry(){
    this.token = new Token().getFromLocalStorage();
    if (this.token == null) {
      this.router.navigate(['/login']);   // navigate to login unless token exists
      return;
    }
    var expiry_date = new Date((this.token.created_at + this.token.expires_in) * 1000);

    if (new Date().getTime() > expiry_date.getTime()){
      console.log('refreshing token');
      this.initiateRefresh(this.token);
    }
    else {
      console.log('token OK');
    }
  }

  initiateRefresh(token){
    this.http.post(this.BASE_URL + '/oauth/token', {
      "refresh_token": token.refresh_token,
      "client_id": environment.client_id,
      "client_secret": environment.client_secret,
      "grant_type": "refresh_token"
      })
      .subscribe(
        suc => {
            console.log('token refreshed');
            this.token = new Token().deserialize(suc);
            this.token.saveToLocalStorage();
        },
        err => {
            console.log('token refreshing failed');
        });;
  }

  // specific methods below //

  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
    this.toastr.success('Logged out successfully');
  }

  signup(user){
    var body = JSON.stringify({user});
    return this.http.post(this.BASE_URL + '/api/v1/users', body, this.HEADERS);
  }

  login(username, password) {

    console.log("in login");
    this.http.post(this.BASE_URL + '/oauth/token', {
      "username": username,
      "password": password,
      "client_id": environment.client_id,
      "client_secret": environment.client_secret,
      "grant_type": "password"
      })
      .subscribe(
        suc => {
            console.log('login successful');
            this.toastr.success('Logged in successfully!');
            this.token = new Token().deserialize(suc);
            this.token.saveToLocalStorage();
            this.router.navigate(['/home']);

        },
        err => {
            console.log('login failed');
            if (err.status == 400){
              this.toastr.error('User not confirmed or bad password!');
            }
        });;
  }

  get(path): Observable<any> {
    return this.http.get(this.BASE_URL + path, { headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'bearer ' + this.token.access_token
      }})
      .pipe(catchError(this.handleError([]))
      );
  }

  post(path, body): Observable<any> {
    return this.http.post(this.BASE_URL + path, body, { headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'bearer ' + this.token.access_token
      }});
  }


  patch(path, body): Observable<any> {
    console.log('patching');
    return this.http.patch(this.BASE_URL + path, body, { headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'bearer ' + this.token.access_token
      }}).
      pipe(catchError(this.handleError([]))
      );
  }

  private handleError<T> (result?: T) {

    return (error: any): Observable<T> => {

      if (error.status == 401){
        console.log(error);
        this.router.navigate(['/login']);
      }
      else if(error.status == 422){
        console.log(error);
      }
      else {
        console.log(error);
        console.log('handle this');
      }
      console.log('ovo je rezultat' + result);
      console.log(error);

      return Observable.throw(error);

  };
}



}
