import { Injectable } from '@angular/core';
import { AuthService } from '@services/auth/auth.service';
import { Observable, of } from 'rxjs';
import { environment } from 'environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private authService: AuthService, private http: HttpClient) { }

  public BASE_URL = environment.baseUrl;

  logout(): void {
    this.authService.logout();
  }

  authorize(): void {
    this.authService.authorize();
  }

  getEvents(): Observable<any> {
    return this.authService.get('/api/v1/events');
  }

  getNews(): Observable<any> {
    return this.authService.get('/api/v1/news');
  }

  getUserNews(id): Observable<any> {
    return this.authService.get('/api/v1/news?user_id=' + id);
  }

  getVenues(): Observable<any> {
    return this.authService.get('/api/v1/venues');
  }

  getEvent(id): Observable<any> {
    return this.authService.get('/api/v1/events/' + id);
  }

  getPerformers(): Observable<any> {
    return this.authService.get('/api/v1/performers');
  }

  getPerformer(id): Observable<any> {
    return this.authService.get('/api/v1/performers/' + id);
  }

  getGroups(): Observable<any> {
    return this.authService.get('/api/v1/groups');
  }

  getActivities(): Observable<any> {
    return this.authService.get('/api/v1/activities');
  }

  getPerformerGroups(id): Observable<any> {
    console.log('jesmo stigli ovdje');
    return this.authService.get('/api/v1/groups?performer_id=' + id);
  }


  getPerformerEvents(id): Observable<any> {
    return this.authService.get('/api/v1/events?performer_id=' + id);
  }

  getGroupEvents(id): Observable<any> {
    return this.authService.get('/api/v1/events?group_id=' + id);
  }

  getGroupPerformers(id): Observable<any> {
    return this.authService.get('/api/v1/performers?group_id=' + id);
  }

  getPerformersNotInGroup(id): Observable<any> {
    return this.authService.get('/api/v1/performers?not_group_id=' + id);
  }

  addPerformerToGroup(g_id, p_id): Observable<any>{
    var obj = { group_performer: { group_id: g_id, performer_id: p_id} };
    var body = JSON.stringify(obj);
    console.log(body);
    return this.authService.post('/api/v1/group_performers', body);

  }

  getEventsForVenue(id): Observable<any> {
    return this.authService.get('/api/v1/events?venue_id=' + id);
  }

  getGroup(id): Observable<any> {
    return this.authService.get('/api/v1/groups/' + id);
  }

  getVenue(id): Observable<any> {
    return this.authService.get('/api/v1/venues/' + id);
  }

  createPerformer(performer): Observable<any> {
    var body = JSON.stringify({performer});
    return this.authService.post('/api/v1/performers', body);
  }

  createGroup(group): Observable<any> {
    var body = JSON.stringify({group});
    return this.authService.post('/api/v1/groups', body);
  }

  createVenue(venue): Observable<any> {
    var body = JSON.stringify({venue});
    return this.authService.post('/api/v1/venues', body);
  }


  getAddressToGeoLocation(address): Observable<any> {
    return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=AIzaSyDuvZ9YTExOlxpEYvLnllnLqTwEFlZY_fs')
  }

  createEvent(event): Observable<any> {
    var body = JSON.stringify({event});
    return this.authService.post('/api/v1/events', body);
  }

  patchEvent(event): Observable<any> {
    return this.authService.patch('/api/v1/events/' + event.id, event);
  }

  addPerformerToEvent(event, participant): Observable<any> {
    var body = JSON.stringify({participant});
    return this.authService.post('/api/v1/events/' + event.id + '/event_participants', body);
  }

  getGroupsExcludingEvent(event){
    return this.authService.get('/api/v1/groups?without_event=' + event.id);
  }

  getPerformersExcludingEvent(event){
    return this.authService.get('/api/v1/performers?without_event=' + event.id);
  }

  getVenueForEvent(event_id){
    return this.authService.get('/api/v1/venues?event_id=' + event_id);
  }

  getUserProfile(user_id){
    return this.authService.get('/api/v1/users/' + user_id);
  }

  getmyRequests(user_id){
    return this.authService.get('/api/v1/tickets?user_id=' + user_id);
  }

  getRequestsforMe(user_id){
    return this.authService.get('/api/v1/tickets?for_user_id=' + user_id);
  }

  confirmRequest(id){
    return this.authService.post('/api/v1/tickets/' + id + '/accept', {});
  }

  rejectRequest(id){
    return this.authService.post('/api/v1/tickets/' + id + '/reject', {});
  }

  request_reservation(body){
    return this.authService.post('/api/v1/tickets', body);
  }

  patchUserPhoto(image, id){
    return this.authService.patch('/api/v1/users/' + id, {user: image});
  }

}
